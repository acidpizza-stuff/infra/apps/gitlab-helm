#!/bin/bash
set -euo pipefail

# Check that gitlab runner version exists first
GITLAB_CHART_VERSION="7.3.2"
# GITLAB_RUNNER_VERSION="15.8.0"

GITLAB_NAMESPACE="gitlab"
ROOT_CA_PATH="${PWD}/files/lan.ca.crt"

# ------------------------------------------------

# Create namespace
kubectl create ns "${GITLAB_NAMESPACE}" --dry-run=client -o yaml | kubectl apply -f -

# Create root ca cert secret
kubectl -n "${GITLAB_NAMESPACE}" create secret generic "gitlab-domain-cert" --from-file="gitlab.k3s.lan.crt"="${ROOT_CA_PATH}" --dry-run=client -o yaml | kubectl apply -f -

# Install Gitlab
helm repo add gitlab "https://charts.gitlab.io"
helm repo update
helm upgrade --install --atomic --debug "gitlab" "gitlab/gitlab" \
  --version "${GITLAB_CHART_VERSION}" \
  --namespace "${GITLAB_NAMESPACE}" \
  --timeout 600s \
  --values "values/external-cert-manager.yml" \
  --values "values/external-ingress-controller.yml" \
  --values "values/gitlab-runner.yml" \
  --values "values/gitlab-ssh.yml" \
  --values "values/monitoring.yml" \
  --values "values/object-store.yml" \
  --values "values/others.yml" \
  --values "values/scale.yml" \
  --values "values/ca-certs.yml"
  # --set gitlab.migrations.enabled=false
  # --set "gitlab-runner.image=gitlab/gitlab-runner:alpine-v${GITLAB_RUNNER_VERSION}"

# Check status
kubectl -n "${GITLAB_NAMESPACE}" rollout status deployment gitlab-minio
kubectl -n "${GITLAB_NAMESPACE}" rollout status deployment gitlab-gitlab-exporter
kubectl -n "${GITLAB_NAMESPACE}" rollout status deployment gitlab-gitlab-runner
kubectl -n "${GITLAB_NAMESPACE}" rollout status deployment gitlab-kas
kubectl -n "${GITLAB_NAMESPACE}" rollout status deployment gitlab-registry
kubectl -n "${GITLAB_NAMESPACE}" rollout status deployment gitlab-gitlab-shell
kubectl -n "${GITLAB_NAMESPACE}" rollout status deployment gitlab-toolbox
kubectl -n "${GITLAB_NAMESPACE}" rollout status deployment gitlab-sidekiq-all-in-1-v2
kubectl -n "${GITLAB_NAMESPACE}" rollout status deployment gitlab-webservice-default
