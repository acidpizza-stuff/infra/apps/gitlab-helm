# Gitlab

## Mapping Guide

https://docs.gitlab.com/charts/installation/version_mappings.html


## Upgrades

Increase Gitlab helm chart version and Gitlab Runner image version.

Typically upgrade from 1.1 -> 1.9 -> 2.0 -> 2.9 -> 3.0 unless otherwise mentioned on upgrade notes.

Need to check if database migrations are necessary: `--set gitlab.migrations.enabled=false`

Reference 
- Upgrade guide: https://docs.gitlab.com/charts/installation/upgrade.html
- Config guide: https://docs.gitlab.com/charts/charts/globals.html
- Values: https://gitlab.com/gitlab-org/charts/gitlab/-/blob/v7.3.2/values.yaml
- gitlab-runner versions: https://hub.docker.com/r/gitlab/gitlab-runner

```bash
# View chart version to Gitlab version mappings
helm repo add gitlab https://charts.gitlab.io/

helm repo update
helm search repo -l gitlab/gitlab | head
helm search repo -l gitlab/gitlab-runner | head

./update.sh
```


## Gitlab Minio

Minio credentials in kubernetes secret.

URL: https://gitlab-minio.k3s.lan

```bash
kubectl -n gitlab get secret gitlab-minio-secret -o jsonpath="{.data.accesskey}" | base64 --decode; echo
kubectl -n gitlab get secret gitlab-minio-secret -o jsonpath="{.data.secretkey}" | base64 --decode; echo
```


## Install CA Cert

Reference: https://docs.gitlab.com/charts/charts/globals.html#custom-certificate-authorities

```bash
kubectl -n gitlab create secret generic secret-custom-ca --from-file=lan.ca.crt=/usr/local/share/ca-certificates/lan.ca.crt
```


## Troubleshooting

Check `gitlab.yml.erb` values.

```bash
helm get -n gitlab manifest gitlab | less
```

### Redis fails to start due to bad file format reading the append only file

Reference: https://stackoverflow.com/questions/68889647/redis-pod-failing

Following the accepted answer's recovery steps resolved the issue.


## Install with Gitlab Runner Manually (OLD)

1. Comment out the gitlab-runner.tf for initial install to install Gitlab alone first via terraform.

2. Create the runner registration token secret (get from admin page runner config).
```bash
kubectl -n gitlab create secret generic gitlab-runner-registration-token --from-literal=registration_token=<token>
```

3. Uncomment out the gitlab runner portions to install gitlab runner via terraform.